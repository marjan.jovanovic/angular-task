import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Employee } from 'src/app/dto/employee.model';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css'],
})
export class AddEmployeeComponent implements OnInit {
  @Input() showModal: boolean = false;
  @Input() editableEmployee: Employee | undefined;
  @Output() closeModal = new EventEmitter();
  @Output() insertEmployee = new EventEmitter<any>();

  employeeForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.employeeForm = this.formBuilder.group(
      {
        name: ['', Validators.required],
        surname: ['', Validators.required],
        office: [''],
        phones: [''],
      },
      { validators: this.officeAndPhonesValidator() }
    );
  }

  onClose(): void {
    this.closeModal.emit();
  }

  onSubmit(): void {
    if (this.employeeForm.valid) {
      if (this.employeeForm.get('phones')!.value !== '') {
        const phonesValue: string = this.employeeForm.get('phones')!.value;
        const phoneArray: string[] = phonesValue.split(',');
        console.log(phoneArray);
        this.employeeForm.get('phones')!.setValue(phoneArray);
      }
      this.insertEmployee.emit(this.employeeForm.value);
      this.employeeForm.reset();
      this.onClose();
    }
  }

  ngOnInit(): void {
    console.log('AddEmployeeComponent initialized');
  }

  officeAndPhonesValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const office = control.get('office');
      const phones = control.get('phones');

      if (office && phones) {
        if (office.value && !phones.value) {
          return {
            officeAndPhones: 'If office is present, phones are required',
          };
        }
      }

      return null;
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['editableEmployee']) {
      console.log('editableEmployee changed', changes['editableEmployee']);

      const newEditableEmployee = changes['editableEmployee'].currentValue;

      if (newEditableEmployee) {
        this.employeeForm.setValue({
          name: newEditableEmployee.name,
          surname: newEditableEmployee.surname,
          office: newEditableEmployee.office,
          phones: newEditableEmployee.phones,
        });
      }
    }
  }
}
