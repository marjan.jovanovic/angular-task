import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Employee } from '../dto/employee.model';
import { EmployeeserviceService } from '../service/employeeservice.service';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css'],
})
export class DashboardHomeComponent implements OnInit {
  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  filtersForm: FormGroup;
  showModal: boolean = false;
  sentEdiableEmployee: any;

  constructor(
    private employeeService: EmployeeserviceService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {
    this.employees = this.employeeService.getMockedEmployees();
    this.filtersForm = this.formBuilder.group({
      officeFilter: [''],
      nameFilter: [''],
    });
    this.filtersForm.get('officeFilter')?.valueChanges.subscribe((value) => {
      console.log('Office filter value changed:', value);
      this.applyFilters();
    });

    this.filtersForm.get('nameFilter')?.valueChanges.subscribe((value) => {
      console.log('Name filter value changed:', value);
      this.applyFilters();
    });
    this.filtersForm.valueChanges
      .pipe()
      .subscribe((value) => this.applyFilters());
  }

  ngOnInit(): void {
    this.filteredEmployees = this.employees;
  }

  openModal(employee?: Employee): void {
    if (employee) {
      console.log('Edit Employee', employee);
    }
    this.showModal = true;
    console.log('Open Modal', this.showModal);
    this.cdr.detectChanges();
  }

  closeModal(): void {
    this.showModal = false;
    console.log('Close Modal', this.showModal);
  }

  applyFilters(): void {
    const { officeFilter, nameFilter } = this.filtersForm.value;

    console.log(
      'Applying filters - Office:',
      officeFilter,
      'Name:',
      nameFilter
    );

    this.filteredEmployees = this.employees.filter(
      (employee) =>
        ((employee.office?.toLowerCase() ?? '').includes(
          officeFilter.toLowerCase()
        ) ||
          officeFilter === '') &&
        (employee.name.toLowerCase().includes(nameFilter.toLowerCase()) ||
          nameFilter === '')
    );
  }

  deleteEmployee(employee: Employee): void {
    const index = this.employees.indexOf(employee);
    if (index !== -1) {
      this.employees.splice(index, 1);
      this.applyFilters();
    }
  }

  editEmployee(employee: Employee): void {
    console.log('Edit Employee', employee);
    this.showModal = true;
    this.sentEdiableEmployee = employee;
  }

  insertEmployee(employee: Employee): void {
    console.log('Insert Employee', employee);
    if (this.sentEdiableEmployee) {
      console.log('Inserting edited Employee', employee);
      const index = this.employees.indexOf(this.sentEdiableEmployee);
      if (index !== -1) {
        this.employees[index] = employee;
        this.sentEdiableEmployee = undefined;
      }
    } else {
      this.employees.push(employee);
    }
    this.applyFilters();
  }
}
