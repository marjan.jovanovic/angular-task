import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardHomeRoutingModule } from './dashboard-home-routing.module';
import { DashboardHomeComponent } from './dashboard-home.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DashboardHomeComponent, AddEmployeeComponent],
  imports: [
    CommonModule,
    DashboardHomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DashboardHomeModule {}
