import { Injectable } from '@angular/core';
import { Employee } from '../dto/employee.model';

@Injectable({
  providedIn: 'root',
})
export class EmployeeserviceService {
  getMockedEmployees(): Employee[] {
    return [
      {
        id: 1,
        name: 'John',
        surname: 'Doe',
        office: 'A1',
        phones: ['123-456-7890', '987-654-3210'],
      },
      {
        id: 2,
        name: 'Jane',
        surname: 'Smith',
        office: 'B2',
        phones: ['555-123-4567'],
      },
    ];
  }

  constructor() {}
}
