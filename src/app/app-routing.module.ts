import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'dashboard-home',
    loadChildren: () =>
      import('./dashboard-home/dashboard-home.module').then(
        (m) => m.DashboardHomeModule
      ),
  },
  { path: '', redirectTo: '/dashboard-home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
