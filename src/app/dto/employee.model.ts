export interface Employee {
  id: number;
  name: string;
  surname: string;
  office?: string;
  phones?: string[];
}
